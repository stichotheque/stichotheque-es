| Poet                 | Title                |   Verses |
| -------------------- | -------------------- | -------- |
| Centenera            | La Argentina         |   10.728 |
| Alonso de Ercilla    | La Araucana          |   21.072 |
| Varios Autores       | Siglo de Oro         |   71.122 |
| Total                |                      |  102.922 |
